<?php 

namespace models;

class Reponse
{
    //Propriétés
    private $id;
    private $texte;
    private $istrue;

    //Méthodes

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of texte
     */ 
    public function getTexte()
    {
        return $this->texte;
    }

    /**
     * Set the value of texte
     *
     * @return  self
     */ 
    public function setTexte($texte)
    {
        $this->texte = $texte;

        return $this;
    }

    /**
     * Get the value of istrue
     */ 
    public function getIstrue()
    {
        return $this->istrue;
    }

    /**
     * Set the value of istrue
     *
     * @return  self
     */ 
    public function setIstrue($istrue)
    {
        $this->istrue = $istrue;

        return $this;
    }
}