<?php 
namespace controllers;

use mysqli;

class ConnexionController{
    // Propriétés
    private $bddUser = 'root';
    private $bddPassword = 'root';
    private $server = "localhost";
    private $bddName = 'php_quiz_objet';
    private $port = 8888;
    public $connexion;
    
    // Constructeur
    public function __construct()
    {
        // On vérifie s'il n'y a pas de connexion existante
        if (!isset($this->connexion)){
            $this->connexion = new mysqli($this->server, $this->bddUser, $this->bddPassword, $this->bddName, $this->port);
            // On déclenche une erreur si on n'est pas connecté
            if(!$this->connexion){
                echo 'Erreur de connexion à la base de données';
                exit;
            }
        }
        // On renvoie la connexion active
        return $this->connexion;
    }
}