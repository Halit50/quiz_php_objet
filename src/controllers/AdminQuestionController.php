<?php

namespace controllers;

use models\Question;
use controllers\ConnexionController;
use models\Reponse;

class AdminQuestionController
{
    // Propriétés
    private $connexion;

    // Constructeur
    public function __construct()
    {
        $this->connexion = new ConnexionController();
    }

    //Méthodes
    public function findAll(){
        // On execute une requete via le ConnexionController instancié dans le 
        // constructeur et sa propriété publique $connexion ($this->connexion qui est un objet mysqli)
        // ensuite on utilise la propriété connexion de mysqli pour executer la requete
        $req = $this->connexion->connexion->query('SELECT * FROM question');
        // On instancie  un tableau qui va recevoir toutes les question de la BDD
        $datas = [];
        // Avec une boucle, on  met en place les questions
        while ($obj = $req->fetch_object()){
            $question = new Question();
            $question->setId($obj->que_id);
            $question->setIntitule($obj->que_intitule);
            array_push($datas, $question);
        }
        return $datas;

    }

    /**
     * Méthode qui supprime une question
     * @param $id int l'id de la question a supprimer
     */
    public function remove(int $id){
        // Suppression des réponses
        $this->connexion->connexion->query('DELETE FROM reponse WHERE rep_question_id = '.$id);
        // Suppression de la question
        return $this->connexion->connexion->query('DELETE FROM question WHERE que_id = '.$id);
    }

    /**
     * Méthode qui permet l'ajout d'une question en bdd
     */
    public function add($post){
        $intitule = $this->conformDataText($post['intitule']);
        $sql = 'INSERT INTO question (que_intitule) VALUES ("'.$intitule.'")';
        $this->connexion->connexion->query($sql);
        //Récupération de l'id
        $id = $this->connexion->connexion->insert_id;
        // Prise en charge des réponses
        $i = 0;
        foreach($post['reponses'] as $reponse){
            $texte = htmlentities(htmlspecialchars(ucfirst($reponse)));
            $result = (!isset($post['results'][$i])) ? 0 : 1;
            $sql = 'INSERT INTO reponse (rep_texte, rep_question_id, rep_istrue) VALUES ("'.$texte.'", '.$id.', '.$result.')';
            $this->connexion->connexion->query($sql);
            $i++;
        }
        return true;
    }

    /**
     * Méthode qui renvoie une question et ses réponses
     */
    public function find($id){
        $sql = 'SELECT * FROM question AS q JOIN reponse AS r ON r.rep_question_id = q.que_id WHERE q.que_id='.$id.' ORDER BY r.rep_id ASC';
        $req = $this->connexion->connexion->query($sql);
        // Mise en place de la question (Objet)
        $question = new Question();
        $question->setId($id);
        $i = 0;
        while ($obj = $req->fetch_object()){
            if($i == 0){
                $question->setIntitule($obj->que_intitule);
            }
            // Instanciation et hydratation de la réponse
            $reponse = New Reponse();
            $reponse->setId($obj->rep_id);
            $reponse->setTexte($obj->rep_texte);
            $reponse->setIstrue($obj->rep_istrue);
            // Ajout de la réponse dans la collection de la question
            $question->addReponse($reponse);
        }
        //
        return $question;
    }

    /**
     * Méthode qui met à jour une question et ses réponses
     */
    public function update($id, $post, $question){
        $reponsesactuelles = [];
        foreach($question->getReponses() as $reponse){
            $rId = $reponse->getId();
            array_push($reponsesactuelles,$rId);
        }
        // Mise à jour de l'intitulé de la question
        $intitule = $this->conformDataText($post['intitule']);
        $sql = 'UPDATE question SET que_intitule="'.$intitule.'" WHERE que_id='.$id;
        $this->connexion->connexion->query($sql);
        //Mise à jour des réponses associées
        // le foreach $key=>$value permet avec $key de récupérer l'id de la réponse
        // on vérifie qu'il y a des reponses associées à la question
        if (!is_null($post['reponses'])) {
            foreach ($post['reponses'] as $key=>$value) {
                // Intitulé de la réponse
                $intitule = $this->conformDataText($value);
                $isTrue = (!isset($post['result'][$key])) ? 0 : 1;
                if (in_array($key, $reponsesactuelles)) {
                    //Mise à jour
                    $sql = 'UPDATE reponse SET rep_texte="'.$intitule.'", rep_istrue='.$isTrue.' WHERE rep_id='.$key;
                    // Supprimer la clé du tableau($reponsesActuelles)
                    array_slice($reponsesactuelles, array_keys($reponsesactuelles, $key)[0], 1);
                } else {
                    // Ajout
                    $sql = 'INSERT INTO reponse (rep_texte, rep_istrue, rep_question_id) VALUES ("'.$intitule.'", '.$isTrue.','.$id.')';
                }
           
                $this->connexion->connexion->query($sql);
                //echo $key.'<br>';
            }
        }
        // Si le count de $reponsesActuelles > 0 avec une boucle on supprime les reponses correspondantes
        if(count($reponsesactuelles)> 0){
            foreach($reponsesactuelles as $id){
                $sql = "DELETE FROM reponse WHERE rep_id=".$id;
                $this->connexion->connexion->query($sql);
            }
        }
        return true;
    }

    /**
     * 
     */
    private function conformDataText($texte){
        return htmlentities(htmlspecialchars(ucfirst($texte)));
    }

    /**
     * Méthode qui renvoie une question et ses réponses
     */
    public function findQuiz(){
        $sql = 'SELECT * FROM question';
        $req = $this->connexion->connexion->query($sql);
        $questionsCollection = [];
        //
        while($obj = $req->fetch_object()){
            $question = $this->find($obj->que_id);
            array_push($questionsCollection, $question);
        }
        //
        return $questionsCollection;
    }
}

