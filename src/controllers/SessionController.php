<?php
namespace controllers;

class SessionController{
    // ##################################################################### //
    // ############################# PROPRIETES ############################ //
    // ##################################################################### //
    private $questionsCollection;
    private $adminQuestionController;
    // ====================================================== //
    // ==================== CONSTRUCTEUR ==================== //
    // ====================================================== //
    public function __construct()
    {
        $this->adminQuestionController = new AdminQuestionController();
        $this->questionsCollection = $this->adminQuestionController->findQuiz();
    }

    /**
     * Get the value of questionsCollection
     */ 
    public function getQuestionsCollection()
    {
        return $this->questionsCollection;
    }

    /**
     * Set the value of questionsCollection
     *
     * @return  self
     */ 
    public function setQuestionsCollection($questionsCollection)
    {
        $this->questionsCollection = $questionsCollection;

        return $this;
    }
}
