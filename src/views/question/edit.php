
<h1>Modifier la question</h1>
<form action="" method="POST">
    <div class="form-group">
        <label for="intitule">Intitulé</label>
        <input type="text" name="intitule" id="intitule" class="form-control" value="<?php echo $question->getIntitule() ?>">
    </div>
    <div class="mt-2">
        <button class="btn btn-dark btn-sm" id="bt-add-reponse" type="button">Ajouter une réponse</button>
    </div>
    <div id="reponses-container">
        <?php 
        //$i = 0;
        foreach($question->getReponses() as $reponse){
            ?>
               <div class="form-group">
                   <label>Intitulé</label>
                   <input type="text" name="reponses[<?php echo $reponse->getId();?>]" value="<?php echo $reponse->getTexte();?>" class="form-control">
                   <div class="checkbox">
                       <input type="checkbox" name="results[<?php echo $reponse->getId();?>]" id="checkbox<?php echo $reponse->getId();?>"<?php if($reponse->getIstrue()) echo "checked";?>>
                       <label for="checkbox<?php echo $reponse->getId();?>">Bonne réponse</label>
                   </div>
               </div>
               <?php
            }
            ?>
    </div>
    <div class="text-end mt-3">
        <button type="submit" class="btn btn-dark">Valider</button>
    </div>

</form>

<script>
    $("#bt-add-reponse").on("click", function(event) {
        // On récup le nb de form-group se trouvant dans le container des réponses pour determiner l'index de la reponse a inserer
        //var num = $("#reponses-container").find(".form-group").length;
        var num = <?php echo $reponse->getId()+1; ?>;
        $("#reponses-container").append('<div class="form-group"><label>Réponse</label><input type="texte" name="reponses[]" class="form-control"><div class="checkbox"><input type="checkbox" name="results['+num+']" id="check'+num+'"><label for="check'+num+'">Bonne réponse</label></div></div>')
    });
</script>