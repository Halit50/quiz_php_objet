<div class="d-flex justify-content-between">
    <h1>Liste des questions</h1>
    <a href="question/new" class="btn btn-success mt-2 mb-2 align-items-center">Nouvelle Question</a>
</div>
<?php 
if (count($questions) == 0){
    echo '<p> Aucune questions dans la base de données </p>';
} else  {
    ?>

    <table class="table table-dark table-striped">
        <thead>
            <tr>
                <th>Id</th>
                <th>Intitulé</th>
                <th class="text-end">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                foreach($questions as $question){
                    echo '<tr><td>'.$question->getId().'</td><td>'.$question->getIntitule().'</td><td class="text-end"><a href="question/delete/'.$question->getId().'" class="btn btn-danger"> Supprimer</a> <a href="question/edit/'.$question->getId().'" class="btn btn-info">Modifier</a></td></tr>';
                }
            ?>
        </tbody>
    </table>

<?php
}

?>