<h1><?php  echo $question->getIntitule(); ?></h1>

<form action="<?php  echo ROOT_DIR.'/quiz/'.($next+1); ?>" method="post">
<?php
foreach($question->getReponses() as $reponse){
    ?>
    <div class="form-group">
        <input type="checkbox" name="reponse" value="<?php echo $reponse->getIsTrue(); ?>">
        <label><?php echo $reponse->getTexte(); ?></label>
    </div>
    <?php
}
?>
<button type="submit" class="btn btn-dark mt-3" >Question suivante</button>
</form>