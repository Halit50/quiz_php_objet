<h1>Ajouter une question</h1>

<form action="" method="POST">
    <div class="form-group">
        <label for="intitule">Intitulé</label>
        <input type="text" name="intitule" id="intitule" class="form-control">
    </div>
    <div class="mt-2">
        <button class="btn btn-dark btn-sm" id="bt-add-reponse" type="button">Ajouter une réponse</button>
    </div>
    <div id="reponses-container">

    </div>
    <div class="text-end mt-3">
        <button type="submit" class="btn btn-dark">Valider</button>
    </div>

</form>

<script>
    $("#bt-add-reponse").on("click", function(event) {
        // On récup le nb de form-group se trouvant dans le container des réponses pour determiner l'index de la reponse a inserer
        var num = $("#reponses-container").find(".form-group").length;
        $("#reponses-container").append('<div class="form-group"><label>Réponse</label><input type="texte" name="reponses[]" class="form-control"><div class="checkbox"><input type="checkbox" name="results['+num+']" id="check'+num+'"><label for="check'+num+'">Bonne réponse</label></div></div>')
    });
</script>