<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php

use controllers\AdminQuestionController;
use models\Reponse;

 include('_head.php') ?>
    <title>Admin</title>
</head>
<body>
    <?php include('_admin-navbar.php') ?>
    <main class="container bg-white shadow">
    <?php 
         if(isset($_SESSION['message'])){
             echo '<div class="alert alert-'.$_SESSION['message'][0].'">'.$_SESSION['message'][1].'</div>';
         } 
         // Suppression du message
         unset($_SESSION['message']);
     ?>
        <?php 
        // On récupère la variabe $uri disponible au niveau d'index.php
        // On l'explose en tableau 
        $toAdmin = explode('/', $uri);
        if (count($toAdmin) == 2){
            echo 'DASHBOARD';
        }else {
            // On vérifie si on a question dans la route
            if (in_array('question', $toAdmin)) {
                //On instancie un controller dédié
                $controller = new AdminQuestionController;
                if (count($toAdmin)>= 4) {
                    if($toAdmin[3]=="delete"){
                        if($controller->remove($toAdmin[4])){
                            // Tout est bon, message et redirection
                            $_SESSION['message'] = ['success', 'Question supprimée!'];
                        }else {
                            $_SESSION['message'] = ['danger', 'Un problème est survenu.'];
                        }
                        header('Location:'.ROOT_DIR.'/admin/question');
                    }
                    if($toAdmin[3]=="new"){
                        // On vérifie si il existe des données postées dans la requête
                        if(!empty($_POST)){
                            //Ajout de la question en BDD
                            if($controller->add($_POST)){
                                $_SESSION['message'] = ['success', 'Votre question a bien été ajoutée'];
                            }else {
                                $_SESSION['message'] = ['danger', 'Votre question n\'a pas été ajoutée'];
                            }
                            header('Location:'.ROOT_DIR.'/admin/question');
                        }
                        //sinon on affiche le formulaire d'ajout
                        require(__DIR__.'/question/new.php');
                    }
                    if($toAdmin[3]=="edit"){
                        $question = $controller->find($toAdmin[4]);
                        if(!empty($_POST)){
                            $controller->update($toAdmin[4], $_POST, $question);
                            //print_r($_POST);
                        }
                        require(__DIR__.'/question/edit.php');
                    }
                    

                } else {
                    $questions = $controller->findAll();
                    require(__DIR__.'/question/index.php');
                }
            }
        }
        ?>
    </main>
    <?php include('_admin-footer.php') ?>
</body>
</html>