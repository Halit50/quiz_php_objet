<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php

use controllers\AdminQuestionController;
use controllers\SessionController;

 include('_head.php') ?>
    <title>Le Quiz</title>
</head>
<body>
    <main class="container bg-white shadow">
        <h1>Jouer au Quiz!</h1>
        <?php   
        $sessionController = new SessionController();
        // if(isset($_POST)){
        //    // On vérifie si on a un score en session
        //    if(isset($_SESSION["score"])){
               
        //    }else{
              
        //    }
        // }
        $toDisplay = explode('/', $uri);
        if(count($toDisplay)==2) {
            $next = 0;
             $_SESSION["score"] = 0;
        }else{
            $_SESSION["score"]+=$_POST["reponse"];
            $next = $toDisplay[2];
            if($toDisplay[2]==count($sessionController->getQuestionsCollection())){
                echo "C'est fini votre score est de :".$_SESSION["score"];
                die();
            }
        } 
        $question = $sessionController->getQuestionsCollection()[$next];
        require __DIR__.'/question/show.php';
        ?>
    </main>
</body>